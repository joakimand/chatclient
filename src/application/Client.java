package application;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Messenger client. Disconnect button does not work. Closing the GUI window
 * will not stop the application from running in Eclipse. Please stop the
 * application from the console.
 * 
 * @author Jocke
 *
 */
public class Client {

	private static final String DEFAULT_IP_ADDRESS = "localhost"; // 127.0.0.1
																	// localhost
																	// 192.168.1.65
	private static final int DEFAULT_PORT_NUMBER = 6677;
	private String ip = "";
	private int port = 0;
	private String username = "";
	private String message;
	private boolean isConnected = false;

	private ClientGUI cGui;
	private Socket socket;
	private ObjectInputStream input;
	private ObjectOutputStream output;

	public Client(ClientGUI cGui) {
		this(DEFAULT_IP_ADDRESS, DEFAULT_PORT_NUMBER, null, cGui);
	}

	public Client(String ip, int port, String username, ClientGUI cGui) {
		this.ip = ip;
		this.port = port;
		this.username = username;
		this.cGui = cGui;
	}

	/*
	 * public static void main(String[] args) { Client client = new Client();
	 * client.connect(); }
	 */

	/**
	 * Try to connect to a server. If successful, open streams. Listen to the
	 * server.
	 */
	public void connect() {
		try {
			socket = new Socket(ip, port);
			// System.out.println("Connected to: " +
			// socket.getInetAddress().getHostAddress() + " at port: " +
			// socket.getPort());
			cGui.displayMessage("Connected to: "
					+ socket.getInetAddress().getHostAddress() + " at port: "
					+ socket.getPort());

			try {
				input = new ObjectInputStream(socket.getInputStream());
				// System.out.println("Input stream open...");
				output = new ObjectOutputStream(socket.getOutputStream());
				// System.out.println("Output stream open...");
				output.flush();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Exception thrown while creating streams.");
			}

			new serverListener().start();

		} catch (Exception e) {
			// cGui.displayMessage("Unable to connect to the server using.\nIP: "
			// + ip + "\nPort: " + port);
//			System.out.println("Unable to connect to the server.");
			cGui.displayMessage("Unable to connect to the server.");
		}

	}

	/**
	 * Disconnects from server.
	 */
	public void disconnect() {
		if (socket != null) {
			isConnected = false;
			try {
				if (input != null)
					input.close();
				if (output != null)
					output.close();
				if (socket != null)
					socket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void setGUI(ClientGUI cGui) {
		this.cGui = cGui;
	}

	/**
	 * Send a message to the server.
	 * 
	 * @param message
	 */
	public void sendMessage(String message) {
		try {
			output.flush();
			output.writeObject(message);
		} catch (IOException e) {
			// System.out.println("Could not write to server.");
			cGui.displayMessage("Could not write to server.");
			// e.printStackTrace();
		}
	}
	
	private void reset() throws IOException {
		disconnect();
		cGui = null;
		isConnected = false;		
	}

	/**
	 * Listens for messages from the server. Sends them to the GUI.
	 * 
	 * @author Jocke
	 *
	 *
	 */
	class serverListener extends Thread {

		@Override
		public void run() {
			isConnected = true;
			try {
				while (isConnected) {

					cGui.displayMessage((String) input.readObject());
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// System.out.println("Server has closed the connection.");
				 cGui.displayMessage("Server has closed the connection.");
				// e.printStackTrace();
			} finally {
				try {
					reset();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} 

		}
	}
}
