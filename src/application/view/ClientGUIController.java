package application.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import application.ClientGUI;

public class ClientGUIController {
	private ClientGUI cGui;

	@FXML
	private Button disconnectFromCurrentButton;
//	Drop button menu?
	@FXML
	private TextArea displayWindow;
	@FXML
	private TextField inputField;
	@FXML
	private TextField ipField;
	@FXML
	private TextField portField;
	@FXML
	private TextField usernameField;
	@FXML
	private MenuItem connectMenuItem;
	@FXML
	private MenuItem disconnectMenuItem;
	
	/**
	 * Reference to the client's GUI.
	 * @param clientGUI
	 */
	public void setGUI(ClientGUI cGui) {
		this.cGui = cGui;
	}
	
	public void initialize() {
	}
	
	/**
	 * Connect to server.
	 * @param event
	 */
	@FXML
	private void connect(ActionEvent event) {
//		Use default.
		cGui.connectToServer();
		inputEnabled(true);
		connectMenuItem.setDisable(true);
		disconnectMenuItem.setDisable(false);
	}
	
	/**
	 * Clears the input field.
	 */
	private void clearInputField() {
		inputField.clear();
	}
	
	/**
	 * Disconnect from server.
	 * @param event
	 */
	@FXML
	private void disconnect(ActionEvent event) {
		cGui.disconnectFromServer();
		inputEnabled(false);
		connectMenuItem.setDisable(false);
		disconnectMenuItem.setDisable(true);
	}
	
	@FXML
	public void onEnterPressed(KeyEvent event) {
		if (event.getCode().equals(KeyCode.ENTER) && !event.isShiftDown()) {
			String message = inputField.getText();
			cGui.sendMessage(message);
			clearInputField();
		}
	}

	/**
	 * Display said message in display window.
	 * @param message
	 */
	public void displayMessage(String message) {
		displayWindow.appendText(message);
	}
	
	/**
	 * 
	 * @param bool
	 */
	public void inputEnabled(boolean bool) {
		inputField.setEditable(bool);
	}

}
