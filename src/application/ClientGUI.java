package application;

import application.view.ClientGUIController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ClientGUI extends Application {

	GridPane root;
	ClientGUIController controller;
	Client client;

	/**
	 * Load the window from FXML. Display it.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource(
					"view/ClientGui.fxml"));
			root = loader.load();

			controller = loader.getController();
			controller.setGUI(this);

			Scene scene = new Scene(root);

			primaryStage.setScene(scene);
			primaryStage.setOnCloseRequest(e -> {

				try {
					client.disconnect();
				} finally {
					System.exit(0);
				}

			});
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Connect using default parameters.
	 */
	public void connectToServer() {
		client = new Client(this);
		client.connect();
	}

	/**
	 * Connect using entered IP address, port number and username.
	 * 
	 * @param ip
	 *            IP to connect to.
	 * @param port
	 *            Port to use.
	 * @param username
	 *            Name to use while chatting.
	 */
	public void connectToServer(String ip, int port, String username) {
		client = new Client(this);
		client.connect();
	}

	/**
	 * Disconnect from current server.
	 */
	public void disconnectFromServer() {
		if (client != null)
			client.disconnect();
		client = null;
	}

	/**
	 * Displays said message in display window.
	 * 
	 * @param message
	 */
	public void displayMessage(String message) {
		controller.displayMessage(message + "\n");
	}

	/**
	 * Sends message to client. Client sends the message to the server.
	 * 
	 * @param message
	 */
	public void sendMessage(String message) {
		client.sendMessage(message);
	}
}
